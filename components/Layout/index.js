import React from "react";

import Link from "next/link";

// import Banner from "/../../assets/img/aaa.png";

const Layout = ({ children }) => {
  return (
    <div>
      {/* <p>Header</p> */}
      <div className="header">
        <div className="header-left"></div>
        <div className="header-right">
          <Link href="/login">
            <a className="header-item">Login / Daftar</a>
          </Link>
          <Link href="/login">
            <a className="header-item">Keranjang</a>
          </Link>
        </div>
      </div>
      <div className="nav-menu">
        <Link href="/">
          <a className="nav-item">Home</a>
        </Link>
        <Link href="/page-2">
          <a className="nav-item">New Arrival</a>
        </Link>
        <Link href="/page-3">
          <a className="nav-item">Tas Besar</a>
        </Link>
        <Link href="/page-4">
          <a className="nav-item">Tas Kecil</a>
        </Link>
        <Link href="/page-5">
          <a className="nav-item">Tas Punggung</a>
        </Link>
        <Link href="/page-6">
          <a className="nav-item">Promo</a>
        </Link>
        <Link href="/">
          <a className="nav-item">Blog</a>
        </Link>
      </div>

      <div className="banner">{/* <img src={Banner} alt="my image" /> */}</div>

      {children}

      <div className="footer">
        <div className="footer-col-1">
          <h4>
            <b>Informasi</b>
          </h4>
          <Link href="/tentang-kami">
            <a className="footer-item">Tentang Kami</a>
          </Link>
          <Link href="/syarat-penggunaan">
            <a className="footer-item">Syarat Penggunaan</a>
          </Link>
          <Link href="/ketentuan-privasi">
            <a className="footer-item">Ketentuan Privasi</a>
          </Link>
          <Link href="/kontak-kami">
            <a className="footer-item">Kontak Kami</a>
          </Link>
          <Link href="/promo-bank">
            <a className="footer-item">Promo Bank</a>
          </Link>
        </div>

        <div className="footer-col-1">
          <h4>
            <b>Bantuan</b>
          </h4>
          <Link href="/bayar-ditempat">
            <a className="footer-item">Bayar Ditempat</a>
          </Link>
          <Link href="/bayar-ditempat">
            <a className="footer-item">Cara Pemesanan</a>
          </Link>
          <Link href="/bayar-ditempat">
            <a className="footer-item">Ketentuan Pengembalian</a>
          </Link>
          <Link href="/bayar-ditempat">
            <a className="footer-item">Ketentuan Pengiriman</a>
          </Link>
        </div>

        <div className="footer-col-1">
          <h4>
            <b>Ikuti Kami</b>
          </h4>
          <Link href="/facebook">
            <a className="footer-item">Facebook Page</a>
          </Link>
          <Link href="/twitter">
            <a className="footer-item">Twitter Page</a>
          </Link>
          <Link href="/instagram">
            <a className="footer-item">Instagram Page</a>
          </Link>
          <Link href="/whatsapp">
            <a className="footer-item">Whatsapp Page</a>
          </Link>
        </div>
      </div>

      {/* <p>footer</p> */}

      <style jsx>
        {`
          body {
            margin: 0;
          }
          .header-right {
            float: right;
          }
          .header-item {
            text-decoration: none;
            color: #777;
            margin: 0 20px;
            position: relative;
            top: -35px;
          }
          .header-item:hover {
            color: #ff8484;
          }
          .nav-menu {
            text-align: center;
            display: block;
            width: 100%;
            margin: auto;
            position: relative;
            padding: 20px 0;
            border-top: 1px solid #666;
            border-bottom: 1px solid #666;
            margin-top: 50px;
          }
          .nav-item {
            padding-left: 3%;
            padding-right: 3%;
            font-size: 13px;
            text-decoration: none;
            color: #777;
          }
          .nav-item:hover,
          .nav-item:active {
            color: #ff8484;
          }
          .banner {
            // background: url("./../");
            backgroundimage: "url('/assets/img/aaa.jpg')";
            height: 100px;
          }
          .footer {
            text-align: center;
          }
          .footer-item {
            text-decoration: none;
            color: #777;
          }
          .footer-item:hover {
            color: #ff8484;
          }
          .footer-col-1 {
            display: inline-grid;
            text-align: left;
            margin: 5px 40px;
          }
        `}
      </style>
    </div>
  );
};

export default Layout;
