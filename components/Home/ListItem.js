import React from "react";

import Link from "next/link";

const ListItem = ({ item }) => {
  return (
    <div className="container">
      <img src="https://picsum.photos/500" />

      <div>
        <p className="category">Style / Bag</p>
        <Link href="/post/[id]" as={`/post/${item.id}`}>
          <a className="title">{item.title}</a>
        </Link>
        <p className="content">{item.content}</p>
      </div>

      <style jsx>
        {`
          .container {
            display: flex;
            margin: 30px;
            align-items: center;
            width: 80%;
          }
          .category {
            font-family: Source Sans Pro;
            font-size: 18px;
            line-height: 24px;
            color: #fb2c74;
          }
          .title {
            font-family: Playfair Display;
            font-size: 28px;
            line-height: 37px;
            letter-spacing: 0.03em;
            color: #373737;
          }
          .content {
            font-family: Source Sans Pro;
            font-size: 18px;
            line-height: 24px;
            letter-spacing: 0.03em;
            color: #373737;
          }
          img {
            width: 200px;
            height: 160px;
            margin-right: 30px;
          }
        `}
      </style>
    </div>
  );
};

export default ListItem;
