import Head from "next/head";

// Function
import fetch from "isomorphic-unfetch";

// Component
import ListItem from "../components/Home/ListItem";

const Index = ({ blogList }) => {
  return (
    <div className="container">
      <Head>
        <title>Tas Keren - Beranda</title>
      </Head>

      {blogList.data.map((blog, key) => {
        return <ListItem key={key} item={blog} />;
      })}

      <style jsx>
        {`
          body {
            background-color: yellow;
            margin: 0;
            padding: 0;
          }
          .container {
            background-color: #E5E5E5;
            margin: 0;
        `}
      </style>
    </div>
  );
};

Index.getInitialProps = async () => {
  const resp = await fetch("http://newsapi.adalfian.site/api/news");
  const data = await resp.json();

  return {
    blogList: data
  };
};

export default Index;
